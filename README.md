# Belajar Maven #

0. Membuat struktur project

    * File `pom.xml`

        ```xml
        <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
            <modelVersion>4.0.0</modelVersion>

            <groupId>com.muhardin.endy.belajar</groupId>
            <artifactId>belajar-maven</artifactId>
            <version>1.0-SNAPSHOT</version>
            <packaging>jar</packaging>

            <properties>
                <maven.compiler.source>11</maven.compiler.source>
                <maven.compiler.target>11</maven.compiler.target>
            </properties>

            <dependencies>
                
            </dependencies>

        </project>
        ```
    
    * Folder `src/main/java`

        [![Struktur folder maven](img/01-struktur-folder.png)](img/01-struktur-folder.png)

1. Compile dan membuat jar

    ```
    mvn package
    ```

    Hasilnya ada di folder `target`

2. Menghapus hasil build / menghapus folder `target`

    ```
    mvn clean
    ```

3. Perintah maven bisa digabungkan

    ```
    mvn clean package
    ```

4. Menambahkan library ke project, buatlah blok `dependency`

    ```xml
    <dependency>
      <groupId>net.sf.jasperreports</groupId>
      <artifactId>jasperreports</artifactId>
      <version>6.18.1</version>
    </dependency>
    ```